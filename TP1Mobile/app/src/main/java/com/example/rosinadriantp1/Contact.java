package com.example.rosinadriantp1;

public class Contact {
    private String pseudo;
    private String nom;
    private int color;

    public int getColor() {
        return color;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getNom() {
        return nom;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Contact(int color, String pseudo, String nom) {
            this.color = color;
            this.pseudo = pseudo;
            this.nom = nom;
        }

}
