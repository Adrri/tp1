package com.example.rosinadriantp1;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends ArrayAdapter<Contact> {

        public ContactAdapter(Context context, List<Contact> lc) {
            super(context, 0, lc);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if(convertView == null){
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_tweet,parent, false);
            }

            TweetViewHolder viewHolder = (TweetViewHolder) convertView.getTag();
            if(viewHolder == null){
                viewHolder = new TweetViewHolder();
                viewHolder.pseudo = (TextView) convertView.findViewById(R.id.pseudo);
                viewHolder.nom = (TextView) convertView.findViewById(R.id.text);
                viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatar);
                convertView.setTag(viewHolder);
            }

            Contact contact = getItem(position);

            viewHolder.pseudo.setText(contact.getPseudo());
            viewHolder.nom.setText(contact.getNom());
            viewHolder.avatar.setImageDrawable(new ColorDrawable(contact.getColor()));

            return convertView;
        }

        private class TweetViewHolder{
            public TextView pseudo;
            public TextView nom;
            public ImageView avatar;
        }

}
