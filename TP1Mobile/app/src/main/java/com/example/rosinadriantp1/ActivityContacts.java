package com.example.rosinadriantp1;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityContacts extends AppCompatActivity {
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        Toast.makeText(getApplicationContext(), "Bienvenue " + MainActivity.nom, Toast.LENGTH_SHORT).show();

        mListView = (ListView) findViewById(R.id.liste);

        List<Contact> contacts = genererContacts();

        ContactAdapter adapter = new ContactAdapter(ActivityContacts.this, contacts);
        mListView.setAdapter(adapter);
    }

    private List<Contact> genererContacts() {
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact(Color.BLUE, "Adrian", "Rosin"));
        contacts.add(new Contact(Color.RED,"Raphael","Castagne"));
        contacts.add(new Contact(Color.YELLOW,"Theo", "Bosetti"));
        contacts.add(new Contact(Color.GREEN,"Kevin", "LePtit"));
        contacts.add(new Contact(Color.MAGENTA,"Maxime", "Revaux"));
        contacts.add(new Contact(Color.GRAY,"Samuel", "Umtiti"));
        contacts.add(new Contact(Color.BLACK,"Cyril", "Hanouna"));
        contacts.add(new Contact(Color.CYAN,"Paco", "Bartes"));
        contacts.add(new Contact(Color.LTGRAY,"Cléa", "Molette"));
        contacts.add(new Contact(Color.WHITE,"David", "Douillez"));
        contacts.add(new Contact(Color.DKGRAY,"Djibril", "Cissé"));
        contacts.add(new Contact(Color.RED,"Franck", "Castle"));
        contacts.add(new Contact(Color.YELLOW,"Matthew", "Murdock"));
        contacts.add(new Contact(Color.MAGENTA,"Dupret", "Antoine"));


        return contacts;
    }
}