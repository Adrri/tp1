package com.example.rosinadriantp1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {
    public static String nom = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText tf = findViewById(R.id.editText);
        final Button button =findViewById(R.id.ButtonValider);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               if(!tf.getText().toString().isEmpty()) {
                   nom = tf.getText().toString();
                   Intent intent = new Intent(MainActivity.this, ActivityContacts.class);
                   startActivity(intent);
               }
            }
        });
    }
}